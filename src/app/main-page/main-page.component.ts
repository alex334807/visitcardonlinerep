import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  nowDate:any = new Date().getTime();
  myBirthday: any = new Date(1997,4,6);
  myFullYears = (this.nowDate - this.myBirthday) / (24 * 3600 * 365.25 * 1000) | 0;
  yearsCall:string;
  schemNum:number = 0;
  longStory:boolean = false;
     
  constructor() { }

  ngOnInit() {
    this.checkYearCall();
  }

  checkYearCall(){
    if(this.myFullYears % 10 > 4 || this.myFullYears % 10 == 0 ){
      this.yearsCall = "лет";
    }
    else{
      if(this.myFullYears % 10 < 5 && this.myFullYears % 10 > 1 ){
        this.yearsCall = "года";
      }
      else{
        this.yearsCall = "год";
      }
    }
  }

  chooseSchemNumber(num:number){
    this.schemNum = num;
  }

  wantLongStory(){
    this.longStory = !this.longStory;
  }

}
